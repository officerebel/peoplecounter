from cozmo import *


robot.world.add_event_handler(cozmo.world.EvtNewCameraImage,
                              my_process_image_function)

def my_process_image_function(event, **kwargs):
  current_image = numpy.array(event.image.raw_image)
  gray = cv2.cvtColor(current_image,cv2.COLOR_BGR2GRAY)
  thresh1 = 100
  ret, thresholded = cv2.threshold(gray, thresh1, 255, 0)
   # more OpenCV calls here


cap = cv2.VideoCapture(0)
# cap = cv2.VideoCapture('peopleCounter.avi')

#Propiedades del video
cap.set(3,160) #Width
cap.set(4,120) #Height